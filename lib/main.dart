import 'package:audioplayers/audio_cache.dart';
import 'package:flutter/material.dart';

void main() => runApp(XylophoneApp());

class XylophoneApp extends StatelessWidget {
  void playSound(int noteNumber) {
    final player = AudioCache();
    player.play('note$noteNumber.wav');
  }

  Expanded placeKey(int keyNumber, Color color) {
    return Expanded(
      child: FlatButton(
        color: color,
        onPressed: () {
          playSound(keyNumber);
        },
      ),
    );
  }

  Column keyboard() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: <Widget>[
        placeKey(1, Colors.red),
        placeKey(2, Colors.orange),
        placeKey(3, Colors.yellow),
        placeKey(4, Colors.green),
        placeKey(5, Colors.blue),
        placeKey(6, Colors.indigo),
        placeKey(7, Colors.purple),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        backgroundColor: Colors.black,
        body: SafeArea(
          child: keyboard(),
        ),
      ),
    );
  }
}
